#This script takes tagged data by nltk/spacy as input, as performed by notebooks 
# ../jupyter_notebooks/Label_NER_with_NLTK.ipynb and ../jupyter_notebooks/Label_NER_with_Spacy.ipynb


# Output: it shows the list of short sentences in the input dataset that are not marked correctly,
# writes final output in a separate file, keeping other sentences and removing the short ones.


input_filename = "../../data/ner/ner_tagged_data_with_nltk_spacy_reviewed_manually.csv"
with open(input_filename, 'r') as f:
	content = f.readlines()

short_sentences=0
sentence_number=0
length=0
min_sentence_length=7 #min length for a sentence to make is valid to keep in dataset

updated_content=[]
new_sentence=[]
for x in range(0,len(content)):
	length=length+1
	new_sentence.append(content[x])
	if content[x].strip()=="BLANK,BLANK,BLANK":
		sentence_number=sentence_number+1
		if length<min_sentence_length:
			short_sentences=short_sentences+1
			print("Length of sentence "+str(sentence_number)+": "+str(length))
			# print(content[x-length:x])
		else:
			updated_content.extend(new_sentence)

		new_sentence = []
		length=0
print("Total short sentences = "+str(short_sentences))

out_filename = "../../data/ner/ner_tagged_data_with_nltk_spacy_reviewed_manually_1.csv"
with open(out_filename, 'w') as out_file:
	out_file.writelines(updated_content)
