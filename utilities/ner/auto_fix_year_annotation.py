#This script takes tagged data by nltk/spacy as input, as performed by notebooks 
# ../jupyter_notebooks/Label_NER_with_NLTK.ipynb and ../jupyter_notebooks/Label_NER_with_Spacy.ipynb


# Output: it shows the list of DATE type named entities detected for mentioned years in the input
# dataset that are not marked correctly, and updates their label with DATE label,
# and writes final output in a separate file


import re
input_filename = "../../data/ner/ner_tagged_data_with_nltk_spacy_reviewed_manually.csv"
with open(input_filename, 'r') as f:
	content = f.readlines()

print(len(content))
out_content=[]
year_count=0

indexes=[]

for x in range(0,len(content)):
	matches = re.findall(',1[8-9][0-9][0-9],O\n', content[x])
	if matches:
		# print(matches)
		if content[x-1].endswith(",In,O\n"):
			year_count=year_count+1
			print(content[x-1])
			print("index: "+str(x)+", value: "+str(content[x]))
			indexes.append(x)
			print("\n\n")




print("total years : "+str(year_count))
print(indexes)



for index in indexes:
	# print(content[index].strip()[0:len(content[index].strip())-1]+"DATE\n")
	content[index] = content[index].strip()[0:len(content[index].strip())-1]+"DATE\n"

out_filename = "../../data/ner/ner_tagged_data_with_nltk_spacy_reviewed_manually_1.csv"
with open(out_filename, 'w') as out_file:
	out_file.writelines(content)

