#This script takes tagged data by nltk/spacy as input, as performed by notebooks 
# ../jupyter_notebooks/Label_NER_with_NLTK.ipynb and ../jupyter_notebooks/Label_NER_with_Spacy.ipynb

# Output: it shows the list of PERSON type named entities detected for different person titles in the input
# dataset that are not marked correctly, and updates their label with PERSON label,
# and writes final output in a separate file


import re
input_filename = "../../data/ner/ner_tagged_data_with_nltk_spacy_reviewed_manually.csv"
with open(input_filename, 'r') as f:
	content = f.readlines()

print(len(content))
out_content=[]
lines = 0
people_count=0

indexes=[]



TITLE = "President" #can change the title to possible titles to detect different PERSON entities, e.g. Mr. Dr. 
print("Finding "+","+str(TITLE)+",O\n\n")
for x in range(0,len(content)):
	if content[x].strip().endswith(","+str(TITLE)+",O"):
		# i=0
		people_count=people_count+1
		print(str(content[x].strip())+" at index: "+str(x))
		
		label_dr_too=False
		for z in range(x+1,x+4):
			if content[z].strip().endswith(","+str(TITLE)+",O") or content[z].strip().startswith("Sentence:"):
				break
			#match for O
			matches = re.findall(',[A-Z]+[a-z]*.,O\n', content[z])
			# matches1 = re.findall(',Dr\.,O\\n,[A-Z][a-z]+,', item1)
			if matches: #names are not labelled as PERSON
				for match in matches:
					print(match.strip())
					indexes.append(z)
					label_dr_too=True
			else:
				#match for PERSON
				matches = re.findall(',[A-Z]+[a-z]*.,PERSON\n', content[z])
				# matches1 = re.findall(',Dr\.,O\\n,[A-Z][a-z]+,', item1)
				if matches:
					label_dr_too=True

		if label_dr_too:
			indexes.append(x)
			

		print("\n\n")





print("total ,"+str(TITLE)+", : "+str(people_count))
print(indexes)

for index in indexes:
	# print(content[index].strip()[0:len(content[index].strip())-1]+"DATE\n")
	content[index] = content[index].strip()[0:len(content[index].strip())-1]+"PERSON\n"

out_filename = "../../data/ner/ner_tagged_data_with_nltk_spacy_reviewed_manually_1.csv"
with open(out_filename, 'w') as out_file:
	out_file.writelines(content)

