#an extension of OptionParser class from optparse package,
#which just ignores the irrelevant flags if given in the command line

from optparse import (OptionParser,BadOptionError,AmbiguousOptionError)

class PassThroughOptionParser(OptionParser):
    """
    An unknown option pass-through implementation of OptionParser.

    When unknown arguments are encountered, bundle with largs and try again,
    until rargs is depleted.  

    sys.exit(status) will still be called if a known argument is passed
    incorrectly (e.g. missing arguments or bad argument types, etc.)        
    """

    def _process_args(self, largs, rargs, values):
        while rargs:
            try:
                OptionParser._process_args(self,largs,rargs,values)
            except (BadOptionError,AmbiguousOptionError) as e:
                largs.append(e.opt_str)

    def validate_opts(self, options, required_opts):
        options_dict = options.__dict__
        for option in required_opts.keys():
            if option not in options_dict.keys() or options_dict[option] is None:
                self.error('Missing REQUIRED parameters: ' + str(required_opts[option]))
