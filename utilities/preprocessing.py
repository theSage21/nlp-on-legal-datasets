#provides data cleaning and pre-processing shared across all datasets preparation for all nlp tasks

import string
import re
#define the special characters we want to remove from plain text
special_characters = string.punctuation
special_characters = special_characters.replace(",","").replace("-","").replace(".","").replace(":","").replace(";","").replace("?","").replace("'","").replace("\"","").replace("!","")
special_characters = special_characters + "¶§•"

#cleans data from 
def clean_text(text):
	text.strip().replace("\n", ". ").strip()
	
	#remove the special characters from text
	text = text.translate(str.maketrans('', '', special_characters))

	text = text.replace("\'\\n\'", ". ")
	text = text.replace("\' \\n\'", ". ")
	text = text.replace("\\n", ". ")
	text = text.replace("¶", "")
	text = text.replace(". , ", ". ")
	text = text.replace(", . ", ". ")
	text = text.replace(". .", ". ")
	text = text.replace(".. ", ". ")
	text = text.replace("..", ". ")
	text = re.sub('[\s]+', ' ', text)
	return text.strip()