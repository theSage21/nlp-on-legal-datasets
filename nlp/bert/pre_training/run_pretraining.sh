fSfTART_TIME=`date +"%Y-%m-%d %T"`
echo "Current Date and Time is: "${START_TIME}


printf "Performing environment checks"

#checkout bert_repo if it does not exist already
[ ! -d bert_repo ] && printf "\n\n<======= downloading bert_repo from git =======>\n\n\n" && git clone https://github.com/google-research/bert bert_repo
#download bert model if it does not exist already
[ ! -f input_model/uncased_L-8_H-512_A-8.zip ] && printf "\n\n<======= downloading bert model  =======>\n\n\n" && wget https://storage.googleapis.com/bert_models/2020_02_20/uncased_L-8_H-512_A-8.zip -P input_model/
#create the input_model/uncased_L-8_H-512_A-8 directory if it does not exist.
[ ! -d input_model/uncased_L-8_H-512_A-8 ] && mkdir input_model/uncased_L-8_H-512_A-8
#extract the download bert model if it is not extracted already
[ ! -f input_model/uncased_L-8_H-512_A-8/vocab.txt ] && printf "\n\n<======= extracting bert model into directory 'input_model/uncased_L-8_H-512_A-8' =======>\n\n\n" && unzip input_model/uncased_L-8_H-512_A-8.zip -d input_model/uncased_L-8_H-512_A-8/

#check if the tf_record files of legal data are availalbe
[ -z "$(ls -A /backup/zohaib/bert_pretraining/tf_record_data/)" ] && printf "FATAL: /backup/zohaib/bert_pretraining/tf_record_data/ directory is empty. Can't proceed. Please provide pretraining legal data in the form of tf_record files in this directory" && exit
#create the /backup/zohaib/bert_pretraining/legal_bert/ directory if it does not exist.
[ ! -d /backup/zohaib/bert_pretraining/legal_bert ] && mkdir /backup/zohaib/bert_pretraining/legal_bert/

printf "\n\n\n Exporting required enviroment variables \n"
export TFRECORD_CHECKPOINT='/backup/zohaib/bert_pretraining/tf_record_data/tf_examples_*'
export PRETRAINING_OUTPUT='/backup/zohaib/bert_pretraining/legal_bert/'
export BERT_MODEL_PATH='./input_model/uncased_L-8_H-512_A-8'

printf "\n\nAll set!!\nStarting the Pretraining process at: \n "
date

python bert_repo/run_pretraining.py \
  --input_file=$TFRECORD_CHECKPOINT \
  --output_dir=$PRETRAINING_OUTPUT \
  --do_train=True \
  --do_eval=True \
  --bert_config_file=$BERT_MODEL_PATH/bert_config.json \
  --init_checkpoint=$BERT_MODEL_PATH/bert_model.ckpt \
  --train_batch_size=16 \
  --max_seq_length=128 \
  --max_predictions_per_seq=20 \
  --num_train_steps=1000000 \
  --num_warmup_steps=20000 \
  --learning_rate=2e-5

printf "\n\n\nDone!!\n"

#print the time stats
printf "\nstart time\n"
echo $START_TIME
printf "\nend time\n"
date