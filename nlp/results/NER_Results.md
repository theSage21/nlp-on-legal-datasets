# NER Results #

Each Model was trained for 5 epochs on the Same NER Dataset.

[NER Tags Population](https://gitlab.com/malik.zohaib90/nlp-on-legal-datasets/-/blob/master/nlp/statistics/ner/ner_tags_population.png?raw=true)


<ins>Note (applicable to results from all models):</ins> 
* Micro average scores in Classification Report are average of scores of all tags including Non-NER i.e. O
* Where as, F1 score in the NER Evaluation Results are average  of f1 scores of only NER tags, i.e. excluding O.



### XLNET_BASE ###
XLNET Base Cased Model, as released by authors.

Average Training and Validation time for 5 Epochs is approx __1 hr 45 mins__. Average is taken from 10 executions.

##### NER Evaluation Results #####

                  Validation Dataset          Test Dataset
                  
    loss          0.049430898745513915        0.05956343201743785
    accuracy      0.990941743098997           0.9896585832801531
    precision     0.8415820642978004          0.8353082603469002
    recall        0.8415820642978004          0.8712942230183609
    f1_score      0.8534048257372655          0.8529218359563369

##### Classification Report on Test Dataset for NER Tags #####

                     precision    recall    f1-score   support

        CARDINAL     0.8868       0.8507    0.8684         221
            DATE     0.9870       0.9940    0.9905        2676
             GPE     0.7178       0.6493    0.6818        2033
               O     0.9979       0.9967    0.9973      295197
    ORGANIZATION     0.8681       0.8425    0.8551        5592
          PERSON     0.8419       0.9202    0.8793        7681

        accuracy                            0.9897      313400
       macro avg     0.8832       0.8755    0.8787      313400
    weighted avg     0.9898       0.9897    0.9897      313400

---


### BERT_BASE ###
Bert Base Cased Model, as released by authors.

Average Training and Validation time for 5 Epochs is approx __1 hr 18 mins__. Average is taken from 10 executions.


##### NER Evaluation Results #####

                  Validation Dataset          Test Dataset
                  
    loss          0.04921574063582354         0.05954885791293772
    accuracy      0.9911184895751141          0.989821314613912
    precision     0.844776119402985           0.8446745562130178
    recall        0.8618664346312813          0.8694133452754143
    f1_score      0.8532357058253471          0.8568654279030763
    

##### Classification Report on Test Dataset for NER Tags #####

                     precision    recall    f1-score   support

        CARDINAL     0.9208       0.8416    0.8794         221
            DATE     0.9870       0.9944    0.9907        2676
             GPE     0.7104       0.6552    0.6817        2033
               O     0.9976       0.9968    0.9972      295197
    ORGANIZATION     0.8706       0.8492    0.8598        5592
          PERSON     0.8569       0.9137    0.8844        7681

        accuracy                            0.9898      313400
       macro avg     0.8905       0.8752    0.8822      313400
    weighted avg     0.9898       0.9898    0.9898      313400


---


### BERT_MEDIUM ###

Bert Medium Model, as released by authors.


Average Training and Validation time for 5 Epochs is approx __30 mins__. Average is taken from 10 executions.


##### NER Evaluation Results #####

                  Validation Dataset          Test Dataset
                  
    loss          0.06598971237909047         0.08259686402948056
    accuracy      0.9863400223458045          0.98351308232291
    precision     0.8290132868656066          0.8178091749520321
    recall        0.8279312595170764          0.8398566950291089
    f1_score      0.8284719198955159          0.8286863152313199

##### Classification Report on Test Dataset for NER Tags #####

                     precision    recall    f1-score   support

        CARDINAL     0.9208       0.8416    0.8794         221
            DATE     0.9845       0.9955    0.9900        2676
             GPE     0.6690       0.6144    0.6405        2033
               O     0.9930       0.9952    0.9941      295197
    ORGANIZATION     0.7739       0.6391    0.7001        5592
          PERSON     0.8302       0.8827    0.8556        7681

        accuracy                            0.9835      313400
       macro avg     0.8619       0.8281    0.8433      313400
    weighted avg     0.9829       0.9835    0.9831      313400

---


### BERT_MEDIUM_LEGAL ###

Bert Medium Model, as released by authors, was further pre-trained on legal dataset using 1 million training steps, to produce BERT_MEDIUM_LEGAL Model.


Average Training and Validation time for 5 Epochs is approx __29.5 mins__. Average is taken from 10 executions.


##### NER Evaluation Results #####

                  Validation Dataset          Test Dataset
                  
    loss          0.06573345777301312         0.08375598771447282
    accuracy      0.9860685902574817          0.9835226547543076
    precision     0.8155631986242476          0.8176326387672912
    recall        0.8253208614313683          0.8364532019704434
    f1_score      0.8204130176235269          0.8269358480541905

##### Classification Report on Test Dataset for NER Tags #####

                     precision    recall    f1-score   support

        CARDINAL     0.8792       0.8235    0.8505         221
            DATE     0.9878       0.9944    0.9911        2676
             GPE     0.6726       0.6114    0.6406        2033
               O     0.9932       0.9951    0.9942      295197
    ORGANIZATION     0.7662       0.6511    0.7040        5592
          PERSON     0.8292       0.8789    0.8533        7681

        accuracy                            0.9835      313400
       macro avg     0.8547       0.8257    0.8389      313400
    weighted avg     0.9830       0.9835    0.9831      313400


---

 

### BERT_MEDIUM_VOCAB ###

Bert Medium Model, as released by authors, was fed with specialized Legal Vocabulary to produce BERT_MEDIUM_VOCAB Model.

Average Training and Validation time for 5 Epochs is approx __2 hrs 5 mins__. Average is taken from 10 executions.

##### NER Evaluation Results #####

                  Validation Dataset          Test Dataset
                  
    loss          0.06479456911504826         0.0786177297460373
    accuracy      0.9863400223458045          0.9837970644543714
    precision     0.8298474945533769          0.8222202751248576
    recall        0.8285838590385034          0.8404836542767578
    f1_score      0.8292151953847828          0.8312516609088494

##### Classification Report on Test Dataset for NER Tags #####

                     precision    recall    f1-score   support

        CARDINAL     0.9347       0.8416    0.8857         221
            DATE     0.9885       0.9948    0.9916        2676
             GPE     0.6604       0.6016    0.6296        2033
               O     0.9932       0.9953    0.9942      295197
    ORGANIZATION     0.7742       0.6518    0.7078        5592
          PERSON     0.8365       0.8866    0.8608        7681

        accuracy                            0.9838      313400
       macro avg     0.8646       0.8286    0.8450      313400
    weighted avg     0.9832       0.9838    0.9834      313400


---


### BERT_MEDIUM_LEGAL_VOCAB ###

Bert Medium Model, as released by authors, was further pre-trained on legal dataset using 1 million training steps, and then fed with specialized Legal Vocabulary to produce BERT_MEDIUM_LEGAL_VOCAB Model.

Average Training and Validation time for 5 Epochs is approx __2 hrs 6 mins__. Average is taken from 10 executions.

##### NER Evaluation Results #####

                  Validation Dataset          Test Dataset
                  
    loss          0.0657837452344351          0.08215550000578425
    accuracy      0.9859865293935702          0.9838385449904276
    precision     0.8140486964016376          0.8132592849704964
    recall        0.8218403306504242          0.8394088669950739
    f1_score      0.817925957999567           0.8261271982017718

##### Classification Report on Test Dataset for NER Tags #####

                     precision    recall    f1-score   support

        CARDINAL     0.8810       0.8371    0.8585         221
            DATE     0.9837       0.9944    0.9890        2676
             GPE     0.6598       0.6114    0.6347        2033
               O     0.9935       0.9953    0.9944      295197
    ORGANIZATION     0.7825       0.6522    0.7114        5592
          PERSON     0.8282       0.8844    0.8554        7681

        accuracy                            0.9838      313400
       macro avg     0.8548       0.8291    0.8406      313400
    weighted avg     0.9833       0.9838    0.9834      313400
    
---



### LEGAL_BERT_BASE_NLPAUEB ###

This model was prepared by [Chalkidis et al](https://arxiv.org/abs/2010.02559#:~:text=We%20also%20propose%20a%20broader,law%2C%20and%20legal%20technology%20applications.). Using Bert Base Model and pre-training it on various legal texts for 1 million training steps


Average Training and Validation time is the same as [BERT_BASE](https://gitlab.com/malik.zohaib90/nlp-on-legal-datasets/-/blob/master/nlp/results/Classification_Results.md#bert_base)

##### NER Evaluation Results #####

                  Validation Dataset          Test Dataset
                  
    loss          0.06751872851897434         0.08742409251565944
    accuracy      0.9869207607673322          0.9843586470963624
    precision     0.8382320441988951          0.8290886502795279
    recall        0.8251033282575593          0.8368114643976713
    f1_score      0.8316158737119053          0.8329321565480967

##### Classification Report on Test Dataset for NER Tags #####

                     precision    recall    f1-score   support

        CARDINAL     0.8591       0.8552    0.8571         221
            DATE     0.9907       0.9944    0.9925        2676
             GPE     0.6764       0.6262    0.6503        2033
               O     0.9934       0.9955    0.9944      295197
    ORGANIZATION     0.7875       0.6760    0.7275        5592
          PERSON     0.8430       0.8750    0.8587        7681

        accuracy                            0.9844      313400
       macro avg     0.8583       0.8370    0.8468      313400
    weighted avg     0.9838       0.9844    0.9840      313400
    
---
